% $Id: introduction.tex 126139 2019-04-01 14:18:38Z yafan $

\section{Introduction}
\label{sec:Introduction}
The \pd{B_c^+}\footnote{Charge conjugate states are implied throughout
  this note, if not specified differently.} meson is the ground state
of the meson family formed by two different heavy flavor quarks, the
anti-\pd{b} quark and the \pd{c} quark.
The mass spectrum of the \pd{B_c} meson family
can be calculated by means of potential models
\cite{Gershtein:1987jj, Chen:1992fq, Eichten:1994gt, Kiselev:1994rc,
  Gupta:1995ps, Fulcher:1998ka, Ebert:2002pp, Godfrey:2004ya}
and lattice QCD~\cite{Davies:1996gi}.
The mass of the ground state \pd{B_c^+} itself has been studied
with pQCD~\cite{Brambilla:2000db} and
lattice QCD~\cite{Shanahan:1999mv, Allison:2004hy, Allison:2004be}.
The unquenched lattice QCD~\cite{Chiu:2007bc}
gave the most accurate prediction of
$\mBc = 6278(6)(4) \mevcc$.

In 1998, the \pd{B_c^+} meson was observed in the semileptonic decay modes
\pd{B_c^+ \rightarrow J/\psi(\mu^+\mu^-) \ell^+ X\ (\ell=e,\mu)}
by CDF at Tevatron~\cite{Abe:1998wi,*Abe:1998fb},
which gave a mass value of $\mBc =6400 \pm 390 \pm 130$ MeV/$c^2$.
In 2005, another collaboration at Tevatron, D0~\cite{bc:2004d0}, reported their preliminary result,
$\mBc =5950^{+140}_{-130} \pm 340$ MeV/$c^2$ using the semileptonic decay \pd{B_c^+ \rightarrow J/\psi(\mu^+\mu^-) \mu^+ X}.
The precision has been improved by the measurements
using the fully reconstructed mode~\cite{Abulencia:2005usa, Aaltonen:2007gv, Abazov:2008kv}
\pd{B_c^+\rightarrow J/\psi(\mu^+\mu^-) \pi^+},
which gave $\mBc = 6275.6 \pm 2.9 \pm 2.5 \mevcc$ by CDF~\cite{Aaltonen:2007gv}
and $\mBc = 6300 \pm 14 \pm 5 \mevcc$ by D0~\cite{Abazov:2008kv}.
At present, the most precise measurement of the $\Bc$ mass
is performed by
LHCb~\cite{LHCb-PAPER-2012-028, LHCb-PAPER-2013-010, LHCb-PAPER-2016-055, LHCb-PAPER-2014-039}
with
$\Bc \to \jpsi \pi^+$,
$\Bc \to \jpsi \Dsp$ with $\Ds \to \phi(K^+K^-)\pip$,
$\Bc \to \jpsi \Dz K^+$,
and
$J/\psi p\bar{p}\pi^+$,
giving
$6273.7 \pm 1.3 \pm 1.6 \mevcc$,
$6276.28 \pm 1.44 \pm 0.36 \mevcc$,
and
$6274.28 \pm 1.40 \pm 0.32 \mevcc$,
separately.
The experimental results of the $\Bc$ mass are summarized
in Table~\ref{tab:ExpResults}.

\begin{table}[bh]
\centering
\caption[Summary of experimental results of the \pd{B_c^+} mass]
{Summary of experimental results of the \pd{B_c^+} mass.
All $\jpsi$ are decayed into $\mumu$.
The first uncertainty is statistical and the second one (if available) is systematic.}
\label{tab:ExpResults}
\begin{minipage}[t]{0.8\linewidth}
\begin{tabular}{l c l c c}
\toprule[1.5pt]
Collab. & $\mathcal{L}_{\rm int}$ $[\invfb]$ & Mode & Signal event & Mass~$[{\rm MeV/}c^2]$ \\
\midrule[1.0pt]
CDF~\cite{Abe:1998wi, *Abe:1998fb} & 0.11  & $\jpsi \ell^+ \nu_\ell$ & $20.4^{+6.2}_{-5.5}$
& $6400 \pm 390 \pm 130$ \\

D0~\cite{bc:2004d0}$^{\dag}$ & 0.21 & $\jpsi \mu^+ X$ & $95 \pm 12 \pm 11$ & $5950^{+140}_{-130}\pm 340$ \\

CDF~\cite{Abulencia:2005usa} & 0.36 & $\jpsi \pi^+$ & $14.6\pm 4.6$ & $6285.7 \pm 5.3 \pm 1.2$ \\

CDF~\cite{Aaltonen:2007gv} & 2.4 & $\jpsi \pi^+$ & $108 \pm 15$ & $6275.6 \pm 2.9 \pm 2.5$ \\

D0~\cite{Abazov:2008kv} & 1.3 & $\jpsi \pi^+$ & $54 \pm 12$ & $6300 \pm 14 \pm 5$ \\

\midrule[1.0pt]

LHCb~\cite{LHCb-PAPER-2012-028}
& 0.37
& $\jpsi \pip$
& $179 \pm 17$
& $6273.7 \pm 1.3 \pm 1.6$\\

LHCb~\cite{LHCb-PAPER-2013-010}
& 3
& $\jpsi \Dsp (\phi\pip)$
& $28.9 \pm 5.6$
& $6276.28 \pm 1.44 \pm 0.36$\\

LHCb~\cite{LHCb-PAPER-2016-055}
& 3
& $\jpsi \Dz K^+$
& $26 \pm 7$
& $6274.28 \pm 1.40 \pm 0.32$\\

LHCb~\cite{LHCb-PAPER-2014-039}
& 3
& $J/\psi p\bar{p}\pi^+$
& $23.9 \pm 5.3$
& $6274.0 \pm 1.8 \pm 0.4$\\

\bottomrule[1.5pt]
\end{tabular}
\\
%\end{center}
\footnotesize
$\dag$: preliminary
\end{minipage}
\end {table}


In this analysis, the $B_c^+$ mass is measured with the $B_c^+$ decays to
$J/\psi \pi^+$,
%$J/\psi K^+$~\cite{LHCb-PAPER-2013-021},
%$\psi(2S) \pi^+$~\cite{LHCb-PAPER-2012-054},
$J/\psi \pi^+ \pi^- \pi^+$~\cite{LHCb-PAPER-2011-044},
$J/\psi p\bar{p}\pi^+$~\cite{LHCb-PAPER-2014-039},
$J/\psi D_s^+$~\cite{LHCb-PAPER-2013-010} with $\DsToKKPi$ and $\DsToTriPi$,
%$J/\psi K^+K^-\pi^+$~\cite{LHCb-PAPER-2013-047},
%$J/\psi 3\pi^+ 2\pi^-$~\cite{LHCb-PAPER-2014-009},
$J/\psi D^0K^+$~ with $D^0 \to \pi^+ K^-$~\cite{LHCb-PAPER-2016-055}, % and $D^0 \to \pi^+ \pi^- \pi^+ K^-$,
and
$B_s^0 \pi^+$~\cite{LHCb-PAPER-2013-044}, with $B_s^0 \to {D_s^- \pi^+, J/\psi
  \phi}$, %and $\DsToKKPi$,
using 3 fb$^{-1}$ of data taken by the LHCb experiment during
2011 and 2012, and 5.5 fb$^{-1}$ of data from 2015 to 2018.
The full decay chains and the
the corresponding $Q$-values (in \mevcc) calculated with the world
averages~\cite{PDG2016}

%3.8 fb-1


\begin{table}[!h]
  \centering
  \caption{\small \label{tab:decay_mode} Full decay chains and the
    corresponding $Q$-values (in \mevcc) calculated with the world averages. All
    $\jpsi$ decays to $\mup\mum$.}
  \small
\begin{tabular}{l|ccc}
\toprule
 Final states & \multicolumn{2}{ c }{Details of intermediates} &
                                                                 Q-values \\
  \hline
 $\jpsi \pi^+ $  &         &\multirow{8}{*}{ $\jpsi \to \mu^+ \mu^-$} &3038\\
 \multirow{2}{*}{$\jpsi 2\pi^+ \pi^-$} & $\psi(2S) \pi^+$ with $\psi(2S) \to \jpsi \pi^+ \pi^-$, &   &\multirow{2}{*}{2759}\\
                                  &or $\jpsi a_1(1260)$ with $a_1(1260) \to \rho(770) \pi^+$     &   &  \\
 $\jpsi p \bar{p} \pi^+$          &   &  &1162\\
 $\jpsi D_s^+(\pi^+ K^+ K^-)$     & $D_s^+ \to \pi^+ \phi(1020)$ with $\phi(1020) \to K^+ K^-$ &  &1210\\
 $\jpsi D_s^+(\pi^+ \pi^+ \pi^-)$ & $D_s^+ \to \pi^+ \pi^+ \pi^-$ &   &1210\\
 $\jpsi D^0(\pi^+ K^-) K^+$       & $D^0 \to \pi^+ K^-$ &  &819\\
 $\Bs(D_s^+ \pi^-) \pi^+$         & $\Bs \to D_s^+ \pi^-$ with $D_s^+ \to K^+ K^- \pi^+$ & &768\\
 $\Bs(\jpsi \phi) \pi^+$          &$\Bs \to \jpsi \phi$ with $\phi \to K^+ K^-$ & &768\\
\bottomrule
\end{tabular}
\end{table}
