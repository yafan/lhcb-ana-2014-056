\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Data and selection}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Data samples}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Cut-based pre-selection}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}BDT-based final selection}{11}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Optimization}{16}{subsection.2.4}
\contentsline {section}{\numberline {3}Signal line shapes}{19}{section.3}
\contentsline {section}{\numberline {4}Mass fit results}{23}{section.4}
\contentsline {section}{\numberline {5}Systematic uncertainties}{28}{section.5}
\contentsline {subsection}{\numberline {5.1}Momentum scale calibration}{28}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Mass fit}{28}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Final state radiation}{30}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Uncertainty on the mass of the intermediate states}{30}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Bias due to lifetime related selection}{31}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}Summary of mass measurement}{31}{subsection.5.6}
\contentsline {section}{\numberline {6}Combination}{36}{section.6}
\contentsline {section}{\numberline {7}Summary}{38}{section.7}
\contentsline {section}{Appendices}{39}{equation.7.1}
\contentsline {section}{\numberline {A}Importance of variables ranked by TMVA}{39}{appendix.A}
\contentsline {section}{\numberline {B}Details for TMVA training}{45}{appendix.B}
\contentsline {section}{\numberline {C}Combination using 0 correlation on signal shape}{48}{appendix.C}
\contentsline {section}{\numberline {D}Combination using decay modes with positive weights}{50}{appendix.D}
\contentsline {section}{\numberline {E}Combination using different information of data}{52}{appendix.E}
\contentsline {section}{\numberline {F}Bias without ${\ensuremath {{\ensuremath {\ensuremath {D}\xspace }}\xspace ^+_{\ensuremath {\ensuremath {s}\xspace }}\xspace }}\xspace $ mass constraint}{54}{appendix.F}
\contentsline {subsection}{\numberline {F.1}Influence in parametrization and FSR}{54}{subsection.F.1}
\contentsline {subsection}{\numberline {F.2}Influence in MC mass fiting}{55}{subsection.F.2}
\contentsline {section}{\numberline {G}Dependence of mass on $\unhbox \voidb@x \hbox {$p_{\rm T}$}\xspace $ and $y$}{59}{appendix.G}
\contentsline {section}{\numberline {H}Further discussion on signal line shape}{60}{appendix.H}
\contentsline {section}{References}{62}{table.caption.88}
