% $Id: introduction.tex 122383 2018-07-26 02:16:41Z yafan $

\section{Introduction}
\label{sec:Introduction}
The \pd{B_c^+}\footnote{Charge conjugate states are implied throughout
  this note, if not specified differently.} meson is the ground state
of the meson family formed by two different heavy flavor quarks, the
anti-\pd{b} quark and the \pd{c} quark.
The mass spectrum of the \pd{B_c} meson family
can be calculated by means of potential models
\cite{Gershtein:1987jj, Chen:1992fq, Eichten:1994gt, Kiselev:1994rc,
  Gupta:1995ps, Fulcher:1998ka, Ebert:2002pp, Godfrey:2004ya}
and lattice QCD~\cite{Davies:1996gi}.
The mass of the ground state \pd{B_c^+} itself has been studied
with pQCD~\cite{Brambilla:2000db} and
lattice QCD~\cite{Shanahan:1999mv, Allison:2004hy, Allison:2004be}.
Recently the unquenched lattice QCD~\cite{Chiu:2007bc}
gave the most accurate prediction of
$\mBc = 6278(6)(4) \mevcc$.

In 1998, the \pd{B_c^+} meson was observed in the semileptonic decay modes
\pd{B_c^+ \rightarrow J/\psi(\mu^+\mu^-) \ell^+ X\ (\ell=e,\mu)}
by CDF at Tevatron~\cite{Abe:1998wi,*Abe:1998fb},
which gave a mass value of $\mBc =6400 \pm 390 \pm 130$ MeV/$c^2$.
In 2005, another collaboration at Tevatron, D0~\cite{bc:2004d0}, reported their preliminary result,
$\mBc =5950^{+140}_{-130} \pm 340$ MeV/$c^2$ using the semileptonic decay \pd{B_c^+ \rightarrow J/\psi(\mu^+\mu^-) \mu^+ X}.
The precision has been improved by the measurements
using the fully reconstructed mode~\cite{Abulencia:2005usa, Aaltonen:2007gv, Abazov:2008kv}
\pd{B_c^+\rightarrow J/\psi(\mu^+\mu^-) \pi^+},
which gave $\mBc = 6275.6 \pm 2.9 \pm 2.5 \mevcc$ by CDF~\cite{Aaltonen:2007gv}
and $\mBc = 6300 \pm 14 \pm 5 \mevcc$ by D0~\cite{Abazov:2008kv}.
At present, the most precise measurement of the $\Bc$ mass
is performed by
LHCb~\cite{LHCb-PAPER-2012-028, LHCb-PAPER-2013-010}
with
$\Bc \to \jpsi \pi^+$
and
$\Bc \to \jpsi \Dsp$ with $\Ds \to \phi(K^+K^-)\pip$,
giving
$6273.7 \pm 1.3 \pm 1.6 \mevcc$
and
$6276.28 \pm 1.44 \pm 0.36 \mevcc$ separately.
The experimental results of the $\Bc$ mass are summarized
in Table~\ref{tab:ExpResults}.

\begin{table}[bh]
\centering
\caption[Summary of experimental results of the \pd{B_c^+} mass]
{Summary of experimental results of the \pd{B_c^+} mass.
All $\jpsi$ are decayed into $\mumu$.
The first uncertainty is statistical and the second one (if available) is systematic.}
\label{tab:ExpResults}
\begin{minipage}[t]{0.8\linewidth}
\begin{tabular}{l c l c c}
\toprule[1.5pt]
Collab. & $\mathcal{L}_{\rm int}$ $[\invfb]$ & Mode & Signal event & Mass~$[{\rm MeV/}c^2]$ \\
\midrule[1.0pt]
CDF~\cite{Abe:1998wi, *Abe:1998fb} & 0.11  & $\jpsi \ell^+ \nu_\ell$ & $20.4^{+6.2}_{-5.5}$
& $6400 \pm 390 \pm 130$ \\

D0~\cite{bc:2004d0}$^{\dag}$ & 0.21 & $\jpsi \mu^+ X$ & $95 \pm 12 \pm 11$ & $5950^{+140}_{-130}\pm 340$ \\

CDF~\cite{Abulencia:2005usa} & 0.36 & $\jpsi \pi^+$ & $14.6\pm 4.6$ & $6285.7 \pm 5.3 \pm 1.2$ \\

CDF~\cite{Aaltonen:2007gv} & 2.4 & $\jpsi \pi^+$ & $108 \pm 15$ & $6275.6 \pm 2.9 \pm 2.5$ \\

D0~\cite{Abazov:2008kv} & 1.3 & $\jpsi \pi^+$ & $54 \pm 12$ & $6300 \pm 14 \pm 5$ \\

\midrule[1.0pt]

LHCb~\cite{LHCb-PAPER-2012-028}
& 0.37
& $\jpsi \pip$
& $179 \pm 17$
& $6273.7 \pm 1.3 \pm 1.6$\\

LHCb~\cite{LHCb-PAPER-2013-010}
& 3
& $\jpsi \Dsp (\phi\pip)$
& $28.9 \pm 5.6$
& $6276.28 \pm 1.44 \pm 0.36$\\

\bottomrule[1.5pt]
\end{tabular}
\\
%\end{center}
\footnotesize
$\dag$: preliminary
\end{minipage}
\end {table}


In this analysis, the $B_c^+$ mass is measured with the $B_c^+$ decays to
$J/\psi \pi^+$,
$J/\psi K^+$~\cite{LHCb-PAPER-2013-021},
$\psi(2S) \pi^+$~\cite{LHCb-PAPER-2012-054},
$J/\psi \pi^+ \pi^- \pi^+$~\cite{LHCb-PAPER-2011-044},
$J/\psi D_s^+$~\cite{LHCb-PAPER-2013-010} with $\DsToKKPi$ and $\DsToTriPi$,
$J/\psi K^+K^-\pi^+$~\cite{LHCb-PAPER-2013-047},
$J/\psi p\bar{p}\pi^+$,
$J/\psi 3\pi^+ 2\pi^-$~\cite{LHCb-PAPER-2014-009},
$J/\psi D^0K^+$~ with $D^0 \to \pi^+ K^-$ and $D^0 \to \pi^+ \pi^- \pi^+ K^-$,
and
$B_s^0 \pi^+$~\cite{LHCb-PAPER-2013-044}, with $B_s^0 \to {J/\psi
  \phi, D_s^- \pi^+}$ and $\DsToKKPi$,
using 3 fb$^{-1}$ of data taken by the LHCb experiment during
2011 and 2012.

