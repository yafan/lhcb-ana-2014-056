\section{Mass Measurement}
\label{sec:massmeasurement}

The $\Bc$ mass is determined from the fit to the following decays using all the Run-I and Run-II data. The $\Bc$ mass is unblinded in the following sections, the fitted $\Bc$ mass in average is 6266MeV$/c^2$ using the following results. The fitted results of each decay were considered , which are shown in:
Fig.~\ref{fig:BcYields} for$\JpsiPi$, $\JpsiTriPi$, $\JpsiKKPi$, $\JpsiDs(\pi^+ K^+ K^-)$, $\JpsiDs(\pi^+ \pi^+ \pi^-)$, $\JpsiPentaPi$, $\BsPiDsPi$, $\BsPiJpsiPhi$, $\Bc \to \jpsi D^0(\pi^+ K^-) K^+$, and $\Bc \to \jpsi p \bar{p} \pi^+$ decays.
Mass resolution compared with MC are shown in:
Fig.~\ref{fig:JpsiD0Kresolution} for $\Bc \to \jpsi D^0(\pi^+ K^-) K^+$ decay.

The mass window of $\Bc$ is tightened to [6150,6550] MeV/$c^2$. As a result of the effect of $B_s^*$, the mass windows of $\Bc$ in $\BsPiJpsiPhi$, $\BsPiDsPi$ decays are tightened to [6200,6550] MeV/$c^2$. The mass windows of $\Bc$ in $\Bc \to \jpsi D^0(\pi^+ K^-) K^+$, $\Bc \to \jpsi p \bar{p} \pi^+$ decays are tighted to [6200,6550] MeV/$c^2$, while in $\JpsiDs(\pi^+ K^+ K^-)$, $\JpsiDs(\pi^+ \pi^+ \pi^-)$ decays are tightened to [6210,6550] MeV/$c^2$.


\begin{figure}
  \centering
  %\subfloat[$\JpsiPi$]{%
  %  \resizebox{0.48\textwidth}{!}{%
  %    \includegraphics{figs/mass/Bc2JpsiPi_All.pdf}}}
  \subfloat[$\JpsiK$]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiK_All.pdf}}}
  \\
  \subfloat[$\PsiPi$]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2PsiPi_All.pdf}}}
  \subfloat[$\JpsiKKPi$]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiKKPi_All.pdf}}}
  \\
  \subfloat[$\JpsiPentaPi$]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiPentaPi_All.pdf}}}
  \caption{The $\Bc$ signal yields in the Run-I and Run-II data.}
  \label{fig:BcYields}
\end{figure}


\addtocounter{figure}{-1}

\begin{figure}
  \centering
  \addtocounter{subfigure}{6}
  \subfloat[$\JpsiDs(K^+K^-\pip)$ (exponential background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiDs_piKK_exp_6210_bdt88.pdf}}}
  \subfloat[$\JpsiDs(K^+K^-\pip)$ (chebychev background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiDs_piKK_che_6210_bdt88.pdf}}}
  \\
  \subfloat[$\JpsiDs(\pi^+\pi^-\pi^+)$ (exponential background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiDs_3pi_exp_6210_bdt98.pdf}}}
  \subfloat[$\JpsiDs(\pi^+\pi^-\pi^+)$ (chebychev background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiDs_3pi_che_6210_bdt98.pdf}}}
  \\
  \subfloat[$\JpsiTriPi$ (exponential background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2Jpsi3pi_exp_6150_bdt25.pdf}}}
  \subfloat[$\JpsiTriPi$ (chebychev background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2Jpsi3pi_che_6150_bdt25.pdf}}}
  %\\
  % \subfloat[$\Bc\to\Bs\pip$, combined fit of $\BsToJpsiPhi$ and $\BsToDsPi$.]{%
  %  \resizebox{0.48\textwidth}{!}{%
  %    \includegraphics{figs/mass/Bc2BsPi_Combined.pdf}}}
  \caption{The $\Bc$ signal yields in the Run-I and Run-II data.}
  \label{fig:BcYields}
\end{figure}

\addtocounter{figure}{-1}

\begin{figure}
  \centering
  \addtocounter{subfigure}{6}
  \subfloat[$\BsPiJpsiPhi$(exponential background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2BsH_JpsiPhi_exp_6200_bdt975.pdf}}}
  \subfloat[$\BsPiJpsiPhi$ (chebychev background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2BsH_JpsiPhi_che_6200_bdt975.pdf}}}
  \\
  \subfloat[$\BsPiDsPi$ (exponential background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2BsH_Dspi_exp_6200_bdt97.pdf}}}
  \subfloat[$\BsPiDsPi$ (chebychev background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2BsH_Dspi_che_6200_bdt97.pdf}}}
  \\
  \subfloat[$\Bc \to \jpsi p \bar{p} \pi^+$ (exponential background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2Jpsipppi_exp_6200_bdt38.pdf}}}
  \subfloat[$\Bc \to \jpsi p \bar{p} \pi^+$ (chebychev background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2Jpsipppi_che_6200_bdt38.pdf}}}
  \caption{The $\Bc$ signal yields in the Run-I and Run-II data.}
  \label{fig:BcYields}
\end{figure}

\addtocounter{figure}{-1}

\begin{figure}
  \centering
  \addtocounter{subfigure}{4}
  \subfloat[$\Bc \to \jpsi D^0(\pi^+ K^-) K^+$ (exponential background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiD0K_pik_exp_6200_bdt33.pdf}}}
  \subfloat[$\Bc \to \jpsi D^0(\pi^+ K^-) K^+$ (chebychev background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiD0K_pik_che_6200_bdt33.pdf}}}
  \\
  \subfloat[$\JpsiPi$ (exponential background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiPi_exp_6150_bdt82.pdf}}}
  \subfloat[$\JpsiPi$ (chebychev background)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/mass/Bc2JpsiPi_che_6150_bdt82.pdf}}}
   \caption{The $\Bc$ signal yields in the Run-I and Run-II data.}
  \label{fig:BcYields}
\end{figure}

\begin{figure}
  \centering
    \includegraphics{figs/Bc2JpsiD0K/JpsiD0K_MCresolution.png}\\
   \caption{The MC resolution for $\Bc \to \jpsi D^0(\pi^+ K^-) K^+$.}
  \label{fig:JpsiD0Kresolution}
\end{figure}

