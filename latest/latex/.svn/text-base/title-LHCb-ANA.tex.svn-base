% $Id$
% ===============================================================================
% Purpose: LHCb-ANA Note title page template
% Author:
% Created on: 2010-10-05
% ===============================================================================

%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%  TITLE PAGE  %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{titlepage}

% Header ---------------------------------------------------
\vspace*{-1.5cm}

\hspace*{-0.5cm}
\begin{tabular*}{\linewidth}{lc@{\extracolsep{\fill}}r}
\ifthenelse{\boolean{pdflatex}}% Logo format choice
{\vspace*{-2.7cm}\mbox{\!\!\!\includegraphics[width=.14\textwidth]{lhcb-logo.pdf}} & &}%
{\vspace*{-1.2cm}\mbox{\!\!\!\includegraphics[width=.12\textwidth]{lhcb-logo.eps}} & &}
 \\
 & & LHCb-ANA-2014-056 \\  % ID
 & & \today \\ % Date - Can also hardwire e.g.: 23 March 2010
 & & v0.1\\
\hline
\end{tabular*}

\vspace*{4.0cm}

% Title --------------------------------------------------
{\bf\boldmath\huge
\begin{center}
  Measurement of $B_c^+$ mass
\end{center}
}

\vspace*{2.0cm}

% Authors -------------------------------------------------
\begin{center}
Yanting Fan, 
Jibo He
\bigskip\\
{\it\footnotesize
University of Chinese Academy of Sciences, Beijing, China\\
}
\end{center}

\vspace{\fill}

% Abstract -----------------------------------------------
\begin{abstract}
  \noindent
In this analysis, the $B_c^+$ mass is measured with the $B_c^+$ decays to
% $B_c^+ \to J/\psi \pi^+$,
% $B_c^+ \to J/\psi K^+$,
% $B_c^+ \to \psi(2S) \pi^+$,
% $B_c^+ \to J/\psi \pi^+ \pi^- \pi^+$,
% $B_c^+ \to J/\psi D_s^+$,
% $B_c^+ \to J/\psi K^+K^-\pi^+$,
% $B_c^+ \to J/\psi p\bar{p}\pi^+$,
% $B_c^+ \to J/\psi 3\pi^+ 2\pi^-$,
% and
% $B_c^+ \to B_s^0 \pi^+$, with $B_s^0 \to J/\psi \phi$
% and with $B_s^0 \to D_s^- \pi^+$,
$J/\psi \pi^+$,
%$J/\psi K^+$,
%$\psi(2S) \pi^+$,
$J/\psi \pi^+ \pi^- \pi^+$,
%$J/\psi K^+K^-\pi^+$,
$J/\psi p\bar{p}\pi^+$,
$J/\psi D^0 K^+$,
$J/\psi D_s^+$, with $D_s^+ \to {K^+ K^- \pip, \pi^+\pi^-\pi^+}$,
and
$B_s^0 \pi^+$, with $B_s^0 \to {J/\psi \phi, D_s^- \pi^+}$,
%and with $B_s^0 \to D_s^- \pi^+$,
using 3 fb$^{-1}$ of data taken by the LHCb experiment during
2011 and 2012 and 5.5 fb$^{-1}$ of data from 2015 to 2018.
%both of RunI and RunII data taken by the LHCb experiment during
%from 2011 to 2018.
\end{abstract}

\vspace*{2.0cm}
\vspace{\fill}

\end{titlepage}


\pagestyle{empty}  % no page number for the title

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%  EOD OF TITLE PAGE  %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  empty page follows the title page ----
\newpage
\setcounter{page}{2}
\mbox{~}

\cleardoublepage
