\newpage
\section{Systematic uncertainties}
\label{sec:systemuncertain}

The mass measurement can be affected by the uncertainty due to
the momentum scale calibration, the fit model (including both signal
and background fit models), the limited knowledge of the intermedidate
state, and by the possible bias caused by the lifetime related
selections.
The influence of the final state radiation as a
correction of the $\Bc$ mass measurement is studied for the signal fit
model, while the correction due to the lifetime bias is also taken into account.


\subsection{Momentum scale calibration}

The dominant term is the momentum scale calibration. For a mass
measurement, the momenta of the final-state particles need to be
measured precisely. In previous studies a large sample of $B^+ \to
\jpsi K^+$, $\jpsi \to \mu^+ \mu^-$ decays was used to calibrate the
track momentum, and the uncertainty on the momentum scale calibration
was determined to be 0.03$\%$,
which is propagated to the $\Bc$ mass by multiplying the $Q$-value of
each decay, as summarized in Table~\ref{tab:Momentum_scale}.
For the influence of particle kinematics, $\pt$ and $y$ is studied in \ref{sec:appendix_ptandeta}.


\begin{table}[!h]
\begin{center}
  \caption{\small \label{tab:Momentum_scale} Systematic uncertainty
    due to the momentum scale calibration uncertainty.}
\begin{tabular}{@{}lcc@{}}
\toprule
Final state & value [MeV$/c^2$]\\
  \midrule
 $\jpsi \pi^+ $ & $\pm$0.91 \\
 $\jpsi 2\pi^+ \pi^-$ & $\pm$0.83 \\
 $\jpsi p \bar{p} \pi^+$ & $\pm$0.35 \\
 $\jpsi D_s^+(\pi^+ K^+ K^-)$ & $\pm$0.36 \\
 $\jpsi D_s^+(\pi^+ \pi^+ \pi^-)$ & $\pm$0.36 \\
 $\jpsi D^0(\pi^+ K^-) K^+$ & $\pm$0.25 \\
 $\Bs(D_s^- \pi^+) \pi^+$ & $\pm$0.23 \\
 $\Bs(\jpsi \phi) \pi^+$ & $\pm$0.23 \\
% $\jpsi D^0(2\pi^+ \pi^- K^-) K^+$ & 0.25MeV$/c^2$ \\
\bottomrule
\end{tabular}
%}
\end{center}
\end{table}

\subsection{Mass fit}

The $\Bc$ mass is determined from the fits to the invariant mass
distribution with signal plus combinatorial background components.

The signal mass distribution is described with a double-sided Crystal
ball function, which may cause bias due to imperfect description of
the signal line shape, especially the effects of the final state radiation.
The correction due to the final state radiation is discussed in
Sec.~\ref{subsec:FSR}.
The fitted mass returned by different signal fit model, including
the Double Crystal Ball, Double-sided Crystal ball, Cruijff functions
are studied in Ref.~\cite{gao:2011-097}.
The results are summarized in Table~\ref{tab:SigLSComp},
one can see that the fitted mass agree with each other
at 0.03\mevcc after correcting the bias caused by the final states
radiation. 
Conservatively, 0.1\mevcc is assigned as systematic uncertainty
due to the signal fit model, to include any other effects that is not
well described by the simulation.

\renewcommand{\arraystretch}{1.3}
\begin{table}[!tb]
\tabcolsep 4mm
\begin{center}
  \caption{\small \label{tab:SigLSComp} Comparison of results of fits
    to $\Bu \to \jpsi K^+$ mass distribution, using the
    Cruijff, Double CB and double-sided CB functions,
    taken from
    Ref.~\cite{gao:2011-097}.
}
\resizebox{1.0\textwidth}{!}{
  \begin{tabular}{lrr|rr}
      \toprule
      & \multicolumn{2}{c}{Mass (in \mevcc)}
      & \multicolumn{2}{c}{Number of signal}
      \\
      \hline
      & Fitted
      & Corrected
      & Fitted
      & Corrected
      \\\midrule

      Cruijff
      &  5279.790 $\pm$ 0.046
      &  5279.790 $\pm$ 0.046
      &  59096 $\pm$ 252
      &  58860 $\pm$ 252 \\

      DoubleCB
      &  5279.461 $\pm$ 0.047
      &  5279.760 $\pm$ 0.047
      &  58918 $\pm$ 252
      &  58950 $\pm$ 252\\

      DSCB
      &  5279.425 $\pm$ 0.047
      &  5279.775 $\pm$ 0.047
      &  58906 $\pm$ 252
      &  58940 $\pm$ 252\\


      \hline
      Double Cruijff
      &  5279.771 $\pm$ 0.046
      &  5279.771 $\pm$ 0.046
      &  59839 $\pm$ 270
      &   -\\

      Double DoubleCB
      &  5279.465 $\pm$ 0.047
      &  5279.765 $\pm$ 0.047
      &  59747 $\pm$ 260
      &  - \\
\bottomrule
\end{tabular}
}
\end{center}
\end{table}


The systematics due to the background line shape has been evaluated by
using an alternate model, second order polynomial (Chebychev
polynomial), instead of the nominal
exponential function. The changes are summarized in
Table~\ref{tab:fitting}.
Conservatively, 0.05\mevcc is assigned as systematic uncertainty
due to the background fit model, to include the on high-statistic $\Bc \to \jpsi \pi^+ $ mode for all the channels and other effects that is not well described by the simulation.
%and are taken as systematic uncertainty.


\begin{table}[!h]
\begin{center}
  \caption{\small \label{tab:fitting} Systematic uncertainty due to
    the background shape.}
\begin{tabular}{@{}lcc@{}}
\toprule
Decay mode & difference [MeV$/c^2$]\\
  \midrule
 $\jpsi \pi^+ $ & $\pm$0.01 \\
 $\jpsi 2\pi^+ \pi^-$ & $\pm$0.01 \\
 $\jpsi p \bar{p} \pi^+$ & $\pm$0.01 \\
 $\jpsi D_s^+(\pi^+ K^+ K^-)$ & $\pm$0.02 \\
 $\jpsi D_s^+(\pi^+ \pi^+ \pi^-)$ & $\pm$0.02 \\
 $\jpsi D^0(\pi^+ K^-) K^+$ & $\pm$0.01 \\
 $\Bs(D_s^+ \pi^-) \pi^+$ & 0 \\
 $\Bs(\jpsi \phi) \pi^+$ & 0 \\
 %$\jpsi D^0(2\pi^+ \pi^- K^-) K^+$ & 0.5MeV$/c^2$ \\
\bottomrule
\end{tabular}
%}
\end{center}
\end{table}


\subsection{Final state radiation}
\label{subsec:FSR}

The final state radiation effect is studied using simulated $\Bc$
signal events. The difference between the fitted $\Bc$ mass
and input mass as a function of mass resolution
are shown in
Figs.~\ref{fig:BcFSR} for all the decay modes used in this analysis.
These are obtained from the fits to the distribution of the $\Bc$ mass
that is calculated with the true momenta of the final states
in the simulated signal samples and smeared by a Gaussian with a
mass resolution in the interested range (covering the expected mass
resolution in data). By doing that, the other effects, e.g., the
momentum scale in the simulation are removed.
The correction on the fitted mass due to the final state
radiations are summarized in Table~\ref{tab:FSR} for all the
decay modes. These are obtained by finding the correction
corresponding to the fitted mass resolution in data in
Figs.~\ref{fig:BcFSR}.

The final state radiations is simulated with \textsc{Photos},
changing the number of low energy photons by $\pm 0.5\%$,
as suggested by the author of the \textsc{Photos},
is found to only change the $\Bu \to \jpsi K^+$
and
$\Bd\to\jpsi\Kstarz$
by $0.01\mevcc$~\cite{Amhis:2011-045}, therefore is negligible for this analysis.



\begin{figure}[hbt]
  \centering
  \subfloat[$\JpsiPi$]{%
    \resizebox{0.4\textwidth}{!}{%
      \includegraphics{figs/FSR/Bc2Jpsipi_BM_DSCB}}}
  \subfloat[$\JpsiTriPi$]{%
    \resizebox{0.4\textwidth}{!}{%
      \includegraphics{figs/FSR/Bc2Jpsi3pi_BM_DSCB}}}
  \\
  \subfloat[$\Bc \to \jpsi p \bar{p} \pi^+$]{%
    \resizebox{0.4\textwidth}{!}{%
      \includegraphics{figs/FSR/Bc2Jpsipppi_BM_DSCB}}}
  \subfloat[$\JpsiDs(\pi^+ K^+ K^-)$]{%
    \resizebox{0.4\textwidth}{!}{%
      \includegraphics{figs/FSR/Bc2JpsiDs_pikk_BM_DSCB}}}
  \\
  \subfloat[$\JpsiDs(\pi^+ \pi^+ \pi^-)$]{%
    \resizebox{0.4\textwidth}{!}{%
      \includegraphics{figs/FSR/Bc2JpsiDs_3pi_BM_DSCB}}}
  \subfloat[$\Bc \to \jpsi D^0(\pi^+ K^-) K^+$]{%
    \resizebox{0.4\textwidth}{!}{%
      \includegraphics{figs/FSR/Bc2JpsiD0K_pik_BM_DSCB}}}
  \\
  \subfloat[$\BsPiDsPi$]{%
    \resizebox{0.4\textwidth}{!}{%
      \includegraphics{figs/FSR/Bc2BsPi_Dspi_BM_DSCB_modify}}}
  \subfloat[$\BsPiJpsiPhi$]{%
    \resizebox{0.4\textwidth}{!}{%
      \includegraphics{figs/FSR/Bc2BsPi_Jpsiphi_BM_DSCB}}}
  %\subfloat[$\Bc \to \jpsi D^0(\pi^+ \pi^+ \pi^- K^-) K^+$]{%
  %  \resizebox{0.4\textwidth}{!}{%
  %    \includegraphics{figs/FSR/Bc2JpsiD0K_k3pi_BM_DSCB}}}
  \caption{Fitted mean $\Bc$ mass as a function of resolution for all decays.
  }
  \label{fig:BcFSR}
\end{figure}



\begin{table}[!h]
\begin{center}
  \caption{\small \label{tab:FSR} Corrections of the fitted mass due to
    the final state radiations.}
\begin{tabular}{@{}lcc@{}}
\toprule
Decay mode & value [MeV$/c^2$]\\
  \midrule
 $\jpsi \pi^+ $ & 0.33\\%$\pm$0.02 \\
 $\jpsi 2\pi^+ \pi^-$ & 0.31\\%$\pm$0.01 \\
 $\jpsi p \bar{p} \pi^+$ & 0.09\\%$\pm$0.02 \\
 $\jpsi D_s^+(\pi^+ K^+ K^-)$ & 0.03\\%$\pm$0.01 \\
 $\jpsi D_s^+(\pi^+ \pi^+ \pi^-)$ & -0.27\\%$\pm$0.02 \\
 $\jpsi D^0(\pi^+ K^-) K^+$ & 0.13\\%$\pm$0.01 \\
 $\Bs(D_s^+ \pi^-) \pi^+$ & 0.10\\%$\pm$0.02 \\
 $\Bs(\jpsi \phi) \pi^+$ & 0.06\\%$\pm$0.01 \\
 %$\jpsi D^0(2\pi^+ \pi^- K^-) K^+$ & 0.5MeV$/c^2$ \\
\bottomrule
\end{tabular}
%}
\end{center}
\end{table}


\subsection{Uncertainty on the mass of the intermediate states}

The limited knowledge on the mass of the intermediate states
can affect the $\Bc$ mass measurement.

The $\jpsi$ mass is known with very good precision,
$3096.900\pm 0.006 \mevcc$~\cite{PDG2016}, and the effects on the $\Bc$ mass can be
neglected.

However, the masses of the $D_s^-$, $D^0$ and $\Bs$ masses
are known with limited precision~\cite{PDG2016},
$m(\Ds) = 1968.34 \pm 0.07 \mevcc$,
$m(\Dz) = 1864.83 \pm 0.05 \mevcc$,
$m(\Bs) = 5366.89 \pm 0.19 \mevcc$
their uncertainties are
propagated to the measurement of $\Bc$ mass as systematic
uncertainty.


\subsection{Bias due to lifetime related selection}

The reconstructed $\Bc$ mass could be biased due to the lifetime
related selections.
Such effects are evaluated by comparing the mass obtained
with fits to the invariant mass distribution before applying any
event selection (we take the reconstructed and MC-matched candidates)
to that after the final selection,
as shown on the left and right hand of
Figs.~\ref{fig:lifetime_selection}, respectively.
The correction and its uncertainty
and uncertainty for each decays are summarized in
Table~\ref{tab:lifetime_bias}. The statistical uncertainty due to the
limited size of simulated signal is taken as the systematic
uncertainty.


 \begin{figure}[!h]
  \centering
  \subfloat[$\JpsiPi$(reconstructed mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/Rec_JpsiPi_DSCB.pdf}}}
  \subfloat[$\JpsiPi$ (cut-added mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/cutadded_JpsiPi_DSCB.pdf}}}
  \\
  \subfloat[$\JpsiTriPi$(reconstructed mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/Rec_Jpsi3pi_DSCB.pdf}}}
  \subfloat[$\JpsiTriPi$ (cut-added mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/cutadded_Jpsi3pi_DSCB.pdf}}}
  \\
  \subfloat[$\Bc \to \jpsi p \bar{p} \pi^+$(reconstructed mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/Rec_Jpsipppi_DSCB.pdf}}}
  \subfloat[$\Bc \to \jpsi p \bar{p} \pi^+$ (cut-added mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/cutadded_Jpsipppi_DSCB_new.pdf}}}
  \\
  \subfloat[$\JpsiDs(K^+K^-\pip)$(reconstructed mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/Rec_JpsiDs_piKK_DSCB.pdf}}}
  \subfloat[$\JpsiDs(K^+K^-\pip)$(cut-added mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/cutadded_JpsiDs_piKK_DSCB.pdf}}}
  \caption{Distribution of difference between the reconstructed mass and the cut-added one, (left)without applying cuts relating to decay time of $\Bc$, (right) after all offline selections relating to $\Bc$ decay time, fitted with DSCB function.}
  \label{fig:lifetime_selection}
\end{figure}

 \begin{figure}[!h]
  \centering
  \addtocounter{subfigure}{8}
  \subfloat[$\JpsiDs(\pi^+\pi^-\pi^+)$ (reconstructed mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/Rec_JpsiDs_3pi_DSCB.pdf}}}
  \subfloat[$\JpsiDs(\pi^+\pi^-\pi^+)$ (cut-added mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/cutadded_JpsiDs_3pi_DSCB.pdf}}}
  \\
  \subfloat[$\Bc \to \jpsi D^0(\pi^+ K^-) K^+$(reconstructed mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/Rec_JpsiD0K_pik_DSCB.pdf}}}
  \subfloat[$\Bc \to \jpsi D^0(\pi^+ K^-) K^+$ (cut-added mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/cutadded_JpsiD0K_pik_DSCB.pdf}}}
   \\
  \subfloat[$\BsPiDsPi$(reconstructed mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/Rec_BsPi_Dspi_DSCB.pdf}}}
  \subfloat[$\BsPiDsPi$ (cut-added mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/cutadded_BsPi_Dspi_DSCB.pdf}}}
  \\
  \subfloat[$\BsPiJpsiPhi$(reconstructed mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/Rec_BsPi_JpsiPhi_DSCB.pdf}}}
  \subfloat[$\BsPiJpsiPhi$ (cut-added mass)]{%
    \resizebox{0.48\textwidth}{!}{%
      \includegraphics{figs/uncertainty/cutadded_BsPi_JpsiPhi_DSCB_new.pdf}}}
  \caption{Distribution of difference between the reconstructed mass and the cut-added one, (left)without applying cuts relating to decay time of $\Bc$, (right) after all offline selections relating to $\Bc$ decay time, fitted with DSCB function.}
  \label{fig:lifetime_selection}
\end{figure}



 \begin{table}[!h]
\begin{center}
  \caption{\small \label{tab:lifetime_bias} Correction and uncertainties of lifetime for all decays.}
\begin{tabular}{@{}lcc@{}}
\toprule
Decay mode & value [MeV$/c^2$]\\
  \midrule
 $\jpsi \pi^+ $ & -0.17$\pm$0.02 \\
 $\jpsi 2\pi^+ \pi^-$ & -0.18$\pm$0.04 \\
 $\jpsi p \bar{p} \pi^+$ & -0.13$\pm$0.03 \\
 $\jpsi D_s^+(\pi^+ K^+ K^-)$ & 0$\pm$0.02 \\
 $\jpsi D_s^+(\pi^+ \pi^+ \pi^-)$ & 0.04$\pm$0.03 \\
 $\jpsi D^0(\pi^+ K^-) K^+$ & -0.11$\pm$0.01 \\
 $\Bs(D_s^+ \pi^-) \pi^+$ & -0.37$\pm$0.10 \\
 $\Bs(\jpsi \phi) \pi^+$ & -0.47$\pm$0.02 \\
 %$\jpsi D^0(2\pi^+ \pi^- K^-) K^+$ & 0.5MeV$/c^2$ \\
\bottomrule
\end{tabular}
%}
\end{center}
\end{table}

\subsection{Summary of mass measurement}
The contributions of the systematic uncertainties from the above
sources, including correction and uncertainties, are summarized
separately in Table~\ref{tab:correction} and
Table~\ref{tab:uncertainties}.



\begin{table}[!h]
\begin{center}
  \caption{\small \label{tab:correction} Correction of different sources for all decays.}
\begin{tabular}{@{}l|cccccc@{}}
\toprule
%&Final states & source & & & & & \\
%  \midrule
%\hline
%\cline{2-7}
 Final states & \multicolumn{2}{ c }{Source [MeV$/c^2$]} \\
%   \midrule
\hline
  & FSR & Selection bias\\%Lifetime \\
  \hline
 $\jpsi \pi^+ $                   & 0.33 &-0.17\\
 $\jpsi 2\pi^+ \pi^-$             & 0.31 &-0.18\\
 $\jpsi p \bar{p} \pi^+$          & 0.09 &-0.13\\
 $\jpsi D_s^+(\pi^+ K^+ K^-)$     & 0.03 &0\\
 $\jpsi D_s^+(\pi^+ \pi^+ \pi^-)$ &-0.27 &0.04\\
 $\jpsi D^0(\pi^+ K^-) K^+$       & 0.13 &-0.11\\
 $\Bs(D_s^+ \pi^-) \pi^+$         & 0.10 &-0.37\\
 $\Bs(\jpsi \phi) \pi^+$          & 0.06 &-0.47\\
 %$\jpsi D^0(2\pi^+ \pi^- K^-) K^+$ & 0.5MeV$/c^2$ \\
\bottomrule
\end{tabular}
%}
\end{center}
\end{table}

\begin{table}[!h]
\begin{center}
  \caption{\small \label{tab:uncertainties} Systematic uncertainties of all decays.}
\begin{tabular}{@{}l|cccccc@{}}
\toprule
%&Final states & source & & & & & \\
%  \midrule
%\hline
%\cline{2-7}
 Final states & \multicolumn{5}{ c }{Source [MeV$/c^2$]} \\
%   \midrule
\hline
  & Momentum & \multirow{2}{*}{Signal} & \multirow{2}{*}{Background} & \multirow{2}{*}{Mass} & \multirow{3}{*}{Lifetime} \\
  & scale & \multirow{2}{*}{shape} & \multirow{2}{*}{shape} & \multirow{2}{*}{uncertainty} & \\
  & calibration &  &  &  & \\
  \hline
 $\jpsi \pi^+ $                   & 0.91 &\multirow{8}{*}{0.1} &\multirow{8}{*}{0.05} &0      &0.02\\
 $\jpsi 2\pi^+ \pi^-$             & 0.83 &                     &                      &0      &0.04\\
 $\jpsi p \bar{p} \pi^+$          & 0.35 &                     &                      &0      &0.03\\
 $\jpsi D_s^+(\pi^+ K^+ K^-)$     & 0.36 &                     &                      &0.07      &0.02\\
 $\jpsi D_s^+(\pi^+ \pi^+ \pi^-)$ & 0.36 &                     &                      &0.07      &0.03 \\
 $\jpsi D^0(\pi^+ K^-) K^+$       & 0.25 &                     &                      &0.05      &0.01\\
 $\Bs(D_s^+ \pi^-) \pi^+$         & 0.23 &                     &                      &0.19   &0.10  \\
 $\Bs(\jpsi \phi) \pi^+$          & 0.23 &                     &                      &0.19   &0.02\\
 %$\jpsi D^0(2\pi^+ \pi^- K^-) K^+$ & 0.5MeV$/c^2$ \\
\bottomrule
\end{tabular}
%}
\end{center}
\end{table}
