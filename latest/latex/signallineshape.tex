\section{Signal line shapes}
\label{sec:signalshape}

The signal line shapes are studied in this section. We use a
Double-sided Crystal Ball function to describe the signal mass distributions,
considering the final state radiations (FSR) and the mass constraints of
the intermediate resonance states.
The Double-sided Crystal Ball function~\cite{Skwarnicki:1986ps} is defined as
\begin{displaymath} \label{eq:DSCB}
\begin{array}{ll}
&F_{\rm DSCB}\left(x;M, \sigma,\alpha_{\rm L},n_{\rm L},
\alpha_{\rm R},n_{\rm R}\right)\\
=&\left\{
\begin{array}{ll}
      e^{-\frac12\alpha_{\rm L}^2}(\frac{n_{\rm L}}{\alpha_{\rm L}})^{n_{\rm L}}
      (\frac{n_{\rm L}}{\alpha_{\rm L}}-\alpha_{\rm L}-\frac{(x-M)}{\sigma})^{-n_{\rm L}}
       &  x<M-\alpha_{\rm L}\sigma, \\
      e^{-\frac12\alpha_{\rm R}^2}(\frac{n_{\rm R}}{\alpha_{\rm R}})^{n_{\rm R}}
      (\frac{n_{\rm R}}{\alpha_{\rm R}}-\alpha_{\rm R}+\frac{(x-M)}{\sigma})^{-n_{\rm R}}
       &  x>M+\alpha_{\rm R}\sigma, \\
       e^{-\frac{(x-M)^2}{2\sigma^2}}
      &  {\rm otherwise, }
\end{array}
\right.
\end{array}
\end{displaymath}
where $M$ is the $\Bc$ mass, $\sigma$
the mass resolution.
As the $\alpha$ and $n$ parameters are correlated,
it is suggested~\cite{Skwarnicki:CB2008} to fix $n$ to some working
value, and fit the $\alpha$. From the physics point of view, $n$
should be fixed to one~\cite{Lefrancois:CB2011}. But the situation is
more complicated when there is mass constraint of the intermediate
state, etc. In this analysis the $n$ parameters are fixed to those obtained from
the fits to the full simulated signal, and also the resolution for each decay are shown in
Table~\ref{tab:n_value}.

With the generator level information in the full simulated samples,
the tail parameters
$\alpha_{\rm L}, \alpha_{\rm R}$ are
parameterized~\cite{Albrecht:1312702,gao:2011-097}
as a function of the mass resolution.
The $\Bc$ mass calculated with the true momenta of the final states
in the simulated signal samples is smeared by a Gaussian with a
mass resolution in the interested range (covering the expected mass
resolution in data).
The process is to fit Roughly with Gaussian function of data and determine the range mass resolution first, and reconstruct with true momenta of not constrainted particles, finally smear with the range of resolution to simulate the detector's influence.
The mass constraints of the intermediate states are also
considered when calculating the $\Bc$ mass.
The resulting mass distributions are then fitted with the
Double-sided Crystal ball function.
The parametrization of $\alpha_{\rm L}, \alpha_{\rm R}$ for each decay are
shown in the following figures:
Figs.~\ref{fig:Bc2JpsiPi_Cruijff} for the $\JpsiPi$ decay,
Figs.~\ref{fig:Bc2Jpsi3Pi_Cruijff} for the $\JpsiTriPi$ decay,
Figs.~\ref{fig:Bc2Jpsipppi_Cruijff} for the $\Bc \to \jpsi p \bar{p} \pi^+$ decay,
Figs.~\ref{fig:Bc2JpsiDs_kkpi_Cruijff} for the $\JpsiDs(\pi^+ K^+ K^-)$ decay,
Figs.~\ref{fig:Bc2JpsiDs_3pi_Cruijff} for the $\JpsiDs(\pi^+ \pi^+ \pi^-)$ decay,
Figs.~\ref{fig:Bc2JpsiD0K_kpi_Cruijff} for the $\Bc \to \jpsi D^0(\pi^+ K^-) K^+$ decay,
Figs.~\ref{fig:Bc2BsPi_DsPi_Cruijff} for the $\BsPiDsPi$ decay,
and
Figs.~\ref{fig:Bc2BsPi_Jpsiphi_Cruijff} for the $\BsPiJpsiPhi$ decay.
%Figs.~\ref{fig:Bc2JpsiD0K_k3pi_Cruijff} for the $\Bc \to \jpsi D^0(\pi^+ \pi^+ \pi^- K^-) K^+$ decay.

\begin{table}[!h]
\begin{center}
  \caption{\small \label{tab:n_value} The value of $n_{\rm L}, n_{\rm R}$ and resolution obtained with the fits to full simulated signal samples.}
\begin{tabular}{@{}l|cccccc@{}}
\toprule
 Final states & $n_{\rm L}$ &  $n_{\rm R}$ & resolution [MeV/$c^2$]\\
%   \midrule
\hline
 $\jpsi \pi^+ $                   & 1.0 &6.0 &13.40\\
 $\jpsi 2\pi^+ \pi^-$             & 2.0 &3.0 &10.13\\
 $\jpsi p \bar{p} \pi^+$          & 2.0 &5.0 &6.26\\
 $\jpsi D_s^+(\pi^+ K^+ K^-)$     & 2.0 &3.0 &5.50\\
 $\jpsi D_s^+(\pi^+ \pi^+ \pi^-)$ & 3.0 &4.0 &5.76\\
 $\jpsi D^0(\pi^+ K^-) K^+$       & 2.6 &4.7 &4.64\\
 $\Bs(D_s^+ \pi^-) \pi^+$         & 2.0 &6.0 &5.48\\
 $\Bs(\jpsi \phi) \pi^+$          & 2.0 &5.0 &5.36\\
 %$\jpsi D^0(2\pi^+ \pi^- K^-) K^+$ & 0.5MeV$/c^2$ \\
\bottomrule
\end{tabular}
%}
\end{center}
\end{table}

%\clearpage
\begin{figure}[hbt]
 \begin{center}
   \subfloat[$\alpha_{\rm L}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Jpsipi_Cruijff_alphaL}}
   \subfloat[$\alpha_{\rm R}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Jpsipi_Cruijff_alphaR}}
   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
     Double-sided Crystal Ball function for $\Bc$ in $\JpsiPi$ decay. }
   \label{fig:Bc2JpsiPi_Cruijff}
 \end{center} \vskip-1cm
\end{figure}

\begin{figure}[!h]
 \begin{center}
   \subfloat[$\alpha_{\rm L}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Jpsi3pi_Cruijff_alphaL}}
   \subfloat[$\alpha_{\rm R}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Jpsi3pi_Cruijff_alphaR}}
   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
     Double-sided Crystal Ball function for $\Bc$ in $\JpsiTriPi$ decay. }
   \label{fig:Bc2Jpsi3Pi_Cruijff}
 \end{center} \vskip-1cm
\end{figure}

\begin{figure}[!h]
 \begin{center}
   \subfloat[$\alpha_{\rm L}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Jpsipppi_Cruijff_alphaL}}
   \subfloat[$\alpha_{\rm R}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Jpsipppi_Cruijff_alphaR}}
   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
     Double-sided Crystal Ball function for $\Bc$ in $\Bc \to \jpsi p \bar{p} \pi^+$ decay.}
   \label{fig:Bc2Jpsipppi_Cruijff}
 \end{center} \vskip-1cm
\end{figure}

\begin{figure}[!h]
 \begin{center}
   \subfloat[$\alpha_{\rm L}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2JpsiDs_kkpi_Cruijff_alphaL}}
   \subfloat[$\alpha_{\rm R}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2JpsiDs_kkpi_Cruijff_alphaR}}
   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
     Double-sided Crystal Ball function for $\Bc$ in $\JpsiDs(\pi^+ K^+ K^-)$ decay.}
   \label{fig:Bc2JpsiDs_kkpi_Cruijff}
 \end{center} \vskip-1cm
\end{figure}


\begin{figure}[!h]
 \begin{center}
   \subfloat[$\alpha_{\rm L}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2JpsiDs_3pi_Cruijff_alphaL}}
   \subfloat[$\alpha_{\rm R}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2JpsiDs_3pi_Cruijff_alphaR}}
   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
     Double-sided Crystal Ball function for $\Bc$ in $\JpsiDs(\pi^+ \pi^+ \pi^-)$ decay.}
   \label{fig:Bc2JpsiDs_3pi_Cruijff}
 \end{center} \vskip-1cm
\end{figure}


\begin{figure}[!h]
 \begin{center}
   \subfloat[$\alpha_{\rm L}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2JpsiD0K_kpi_Cruijff_alphaL}}
   \subfloat[$\alpha_{\rm R}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2JpsiD0K_kpi_Cruijff_alphaR}}
   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
     Double-sided Crystal Ball function for $\Bc$ in $\Bc \to \jpsi D^0(\pi^+ K^-) K^+$ decay.}
   \label{fig:Bc2JpsiD0K_kpi_Cruijff}
 \end{center} \vskip-1cm
\end{figure}

\begin{figure}[!h]
 \begin{center}
   \subfloat[$\alpha_{\rm L}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Bspi_DsPi_Cruijff_alphaL}}
   \subfloat[$\alpha_{\rm R}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Bspi_DsPi_Cruijff_alphaR}}
   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
     Double-sided Crystal Ball function for $\Bc$ in $\BsPiDsPi$ decay.}
   \label{fig:Bc2BsPi_DsPi_Cruijff}
 \end{center} \vskip-1cm
\end{figure}

\begin{figure}[!h]
 \begin{center}
   \subfloat[$\alpha_{\rm L}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Bspi_JpsiPhi_Cruijff_alphaL}}
   \subfloat[$\alpha_{\rm R}$]
   {\includegraphics[scale=0.35]{figs/parametrization/Bc2Bspi_JpsiPhi_Cruijff_alphaR}}
   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
     Double-sided Crystal Ball function for $\Bc$ in $\BsPiJpsiPhi$ decay.}
   \label{fig:Bc2BsPi_Jpsiphi_Cruijff}
 \end{center} \vskip-1cm
\end{figure}

%\begin{figure}
% \begin{center}
%   \subfloat[$\alpha_{\rm L}$]
%   {\includegraphics[scale=0.35]{figs/parametrization/Bc2JpsiD0K_k3pi_Cruijff_alphaL}}
%   \subfloat[$\alpha_{\rm R}$]
%   {\includegraphics[scale=0.35]{figs/parametrization/Bc2JpsiD0K_k3pi_Cruijff_alphaR}}
%   \caption{Parametrization of $\alpha_{\rm L}$, $\alpha_{\rm R}$ of
%     Double-sided Crystal Ball function for $\Bc$ in $\Bc \to \jpsi D^0(\pi^+ \pi^+ \pi^- K^-) K^+$ decay.}
%   \label{fig:Bc2JpsiD0K_k3pi_Cruijff}
% \end{center} \vskip-1cm
%\end{figure}



In the following sections, the $\Bc$ signal is described by a Double-sided Crystal Ball function, and the
background by exponential function as the nominal or the second-order polynomial
function (Chebychev polynomial) as the alternative. Also, the effects of different signal
line shapes and background line shapes on the mass measurement are considered
separately in the section of systematic uncertainties.

